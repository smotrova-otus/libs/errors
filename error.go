package errors

import (
	"encoding/json"
	"net/http"
)

type Error struct {
	Err    string `json:"error,omitempty"`
	Code   string `json:"code,omitempty"`
	Status int    `json:"-"`
}

func (e *Error) MarshalJSON() ([]byte, error) {
	type t struct {
		Err    string `json:"error,omitempty"`
		Code   string `json:"code,omitempty"`
		Status int    `json:"-"`
	}

	return json.Marshal((*t)(e))
}

func (e *Error) Error() string {
	return e.Err
}

func (e *Error) StatusCode() int {
	return e.Status
}

var (
	ErrInternal = &Error{
		Err:    "internal server error",
		Code:   "INTERNAL_SERVER_ERROR",
		Status: http.StatusInternalServerError,
	}
	ErrBadRequest = &Error{
		Err:    "bad request",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	ErrInvalidPathParameters = &Error{
		Err:    "invalid path parameters",
		Code:   "INVALID_PATH_PARAMETERS",
		Status: http.StatusBadRequest,
	}
	ErrInvalidQueryParameters = &Error{
		Err:    "invalid query parameters",
		Code:   "INVALID_QUERY_PARAMETERS",
		Status: http.StatusBadRequest,
	}
	ErrInvalidBody = &Error{
		Err:    "request body is invalid",
		Code:   "INPUT_DATA_INVALID_BODY",
		Status: http.StatusBadRequest,
	}
	ErrUnauthorized = &Error{
		Err:    "Unauthorized",
		Code:   "UNAUTHORIZED",
		Status: http.StatusUnauthorized,
	}
	ErrForbidden = &Error{
		Err:    "Forbidden",
		Code:   "FORBIDDEN",
		Status: http.StatusForbidden,
	}
	ErrNotFound = &Error{
		Err:    "not found",
		Code:   "NOT_FOUND",
		Status: http.StatusNotFound,
	}
)
